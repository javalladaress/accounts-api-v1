#
# Build stage
#
FROM maven:3.6.3-adoptopenjdk-8 AS build
COPY src /api/src/
COPY pom.xml /api/
RUN mvn -e -U -f /api/pom.xml -P pro clean package

#
# Package stage
#
FROM openjdk:8-alpine
COPY --from=build /api/target/lib/*.jar /usr/local/lib/
COPY --from=build /api/target/accounts-api-v1-1.0.0.jar /usr/local/lib/accounts-api-v1.jar
EXPOSE 3001
ENTRYPOINT ["java", "-cp", "/usr/local/lib/*", "com.bbva.practitioner.accounts.v1.AccountsV1"]