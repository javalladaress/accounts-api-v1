package com.bbva.practitioner.accounts.v1.rest;

import com.google.gson.Gson;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;

import spark.Request;

public class HttpRequest {

	private final Gson gson;
	private final Request request;
	
	@AssistedInject
	HttpRequest(Gson gson, @Assisted Request request) {
		this.gson = gson;
		this.request = request;
	}
	
	public String body() {
		return this.request.body();
	}
	
	public <T> T body(Class<T> bodyType) {
		return this.gson.fromJson(request.body(), bodyType);
	}
	
	public String pathParam(final String name) {
		return this.request.params(name);
	}
	
	public String queryParam(final String name) {
		return this.request.queryParams(name);
	}
	
	public String header(final String name) {
		return this.request.headers(name);
	}
	
}
