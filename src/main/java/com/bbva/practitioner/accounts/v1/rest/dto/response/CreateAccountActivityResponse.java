package com.bbva.practitioner.accounts.v1.rest.dto.response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CreateAccountActivityResponse")
public class CreateAccountActivityResponse extends APIResponse {

	@Expose
	@Since(1.0)
	@SerializedName("activity")
	@XStreamAlias("activity")
	final AccountActivity activity;
	
	private CreateAccountActivityResponse(final @Nonnull String message, final @Nullable AccountActivity activity, final Boolean success) {
		super(success, message);
		this.activity = activity;
	}
	
	public static CreateAccountActivityResponse ok(final String message, final AccountActivity activity) {
		return new CreateAccountActivityResponse(message, activity, Boolean.TRUE);
	}
	
	public static CreateAccountActivityResponse ko(final String message) {
		return new CreateAccountActivityResponse(message, null, Boolean.FALSE);
	}
	
}
