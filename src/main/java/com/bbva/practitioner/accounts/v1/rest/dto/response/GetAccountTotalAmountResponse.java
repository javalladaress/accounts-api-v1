package com.bbva.practitioner.accounts.v1.rest.dto.response;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("GetAccountActivityResponse")
public class GetAccountTotalAmountResponse extends APIResponse {
	
	@Expose
	@Since(1.0)
	@SerializedName("data")
	@XStreamAlias("data")
	private Map<String, Object> totalAmount;
	
	private GetAccountTotalAmountResponse(final String message, final @Nullable Map<String, Object> totalAmount, final Boolean success) {
		super(success, message);
		this.totalAmount = totalAmount;
	}
	
	@SuppressWarnings("unchecked")
	public static GetAccountTotalAmountResponse ok(final Map<String, Object> totalAmount) {
		final List<Object> data = (List<Object>) ((Map<String, Object>) totalAmount.get("cursor")).get("firstBatch");
		
		if( ( data == null ) || data.isEmpty() ) {
			return new GetAccountTotalAmountResponse("no data found", Collections.emptyMap(), Boolean.TRUE);
		}
		
		return new GetAccountTotalAmountResponse("found", (Map<String, Object>) data.get(0), Boolean.TRUE);
	}
	
	public static GetAccountTotalAmountResponse ko() {
		return new GetAccountTotalAmountResponse("not found", null, Boolean.FALSE);
	}
	
}
