package com.bbva.practitioner.accounts.v1.rest.dto.request;

import java.util.Map;

import com.bbva.practitioner.accounts.v1.data.DaaSBodyProvider;
import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UpdateAccountRequest")
public class UpdateAccountRequest implements Supplier<Account>, DaaSBodyProvider<Map<String, Object>> {
	
	@Expose
	@Since(1.0)
	@SerializedName("account")
	@XStreamAlias("account")
	private final Account account;
	
	private UpdateAccountRequest(final Account account) {
		this.account = account;
	}
	
	public static UpdateAccountRequest of(final Account account) {
		return new UpdateAccountRequest(account);
	}
	
	@Override
	public Map<String, Object> getDaaSBody() {
		return ImmutableMap.of("$set", this.account);
	}

	@Override
	public Account get() {
		return this.account;
	}
	
}
