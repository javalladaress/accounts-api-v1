package com.bbva.practitioner.accounts.v1.data;

import java.io.IOException;

import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Singleton
@javax.inject.Named(AccountsDaaS.TAG)
class AccountsDaaS extends DaaS<Accounts> implements Interceptor {
	
	protected static final String TAG = "accountsDaaS";
	
	private final String apiKey;
	
	@Inject
	AccountsDaaS(@Named("api.daas.endpoint") String daaSEndpoint, @Named("api.daas.key") String apiKey) {
		super(daaSEndpoint);
		this.apiKey = apiKey;
	}
	
	protected OkHttpClient.Builder getClient(final OkHttpClient.Builder builder) {
		return builder.addInterceptor(this);
	}

	@Override
	public Response intercept(final Chain chain) throws IOException {
        final Request original = chain.request();
        final HttpUrl originalHttpUrl = original.url();
        
        final HttpUrl url = setApiKey(originalHttpUrl.newBuilder()).build();
        final Request.Builder requestBuilder = setAcceptHeader(original.newBuilder().url(url));
        
        requestBuilder.tag(TAG);
        
        return chain.proceed(requestBuilder.build());
	}
	
	private HttpUrl.Builder setApiKey(final HttpUrl.Builder builder) {
		return builder.addQueryParameter("apiKey", this.apiKey);
	}
	
	private Request.Builder setAcceptHeader(final Request.Builder builder) {
		return builder.addHeader(HttpHeaders.ACCEPT, MediaType.JSON_UTF_8.toString());
	}

	@Override
	public Accounts get() {
		return getDataAPI(Accounts.class);
	}
	
	public AccountActivities forActivity() {
		return getAPI(AccountActivities.class);
	}
	
}
