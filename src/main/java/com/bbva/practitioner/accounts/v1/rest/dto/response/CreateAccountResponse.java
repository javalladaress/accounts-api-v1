package com.bbva.practitioner.accounts.v1.rest.dto.response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CreateAccountResponse")
public class CreateAccountResponse extends AccountsAPIResponse {

	private CreateAccountResponse(final @Nonnull String message, final @Nullable Account account, final Boolean success) {
		super(message, account, success);
	}
	
	public static CreateAccountResponse ok(final String message, final Account account) {
		return new CreateAccountResponse(message, account,Boolean.TRUE);
	}
	
	public static CreateAccountResponse ko(final String message) {
		return new CreateAccountResponse(message, null, Boolean.FALSE);
	}
	
}
