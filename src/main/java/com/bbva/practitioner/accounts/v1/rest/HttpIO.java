package com.bbva.practitioner.accounts.v1.rest;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.google.common.base.Charsets;
import com.google.common.base.Supplier;
import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.thoughtworks.xstream.XStream;

import spark.Response;

@Named("httpIO")
@Singleton
public class HttpIO implements Supplier<Gson> {

	private final Gson gson;
	private final XStream xstream;
	
	@Inject
	HttpIO(Gson gson, XStream xstream) {
		this.gson = gson;
		this.xstream = xstream;
	}
	
	public void registerType(final Class<?> clazz) {
		this.xstream.processAnnotations(clazz);
	}
	
	public void toJson(final Response response, APIResponse apiResponse) {
		response.type(MediaType.JSON_UTF_8.toString());
		try(final Writer writer = getResponseWriter(response)) {
			this.gson.toJson(apiResponse, writer);
			writer.flush();
		} catch(Exception e) {
			e.printStackTrace(System.err);
		}
	}
	
	public void toXml(final Response response, APIResponse apiResponse) {
		response.type(MediaType.XML_UTF_8.toString());
		try(final Writer writer = getResponseWriter(response)) {
			this.xstream.toXML(apiResponse, writer);
			writer.flush();
		} catch(Exception e) {
			e.printStackTrace(System.err);
		}
	}
	
	private Writer getResponseWriter(final Response response) throws IOException {
		return new OutputStreamWriter(response.raw().getOutputStream(), Charsets.UTF_8);
	}

	@Override
	public Gson get() {
		return this.gson;
	}
}
