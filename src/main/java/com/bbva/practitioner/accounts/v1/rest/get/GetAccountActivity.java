package com.bbva.practitioner.accounts.v1.rest.get;

import java.io.IOException;
import java.util.List;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountActivityService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountActivityResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(GetAccountActivity.NAME)
class GetAccountActivity extends Controller<GetAccountActivityResponse> {

	protected static final String NAME = "getAccountActivity";

	private final AccountActivityService activity;
	
	@Inject
	GetAccountActivity(@Named("api.endpoint." + NAME) String endpoint, AccountActivityService activity) {
		super(NAME, endpoint);
		this.activity = activity;
	}
	
	@Override
	protected void init(HttpIO httpIO) {
		httpIO.registerType(GetAccountActivityResponse.class);
	}
	
	@Override
	public GetAccountActivityResponse process(final HttpRequest request, final HttpResponse response) throws IOException {		
		final List<AccountActivity> activities = this.activity.getActivity(
				request.pathParam("accountId"), getLongParam(request, "first", 0L), getLongParam(request, "size", 1L));
		return GetAccountActivityResponse.ok(activities);
	}

}
