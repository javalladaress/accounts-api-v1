package com.bbva.practitioner.accounts.v1.rest;

import static spark.Spark.*;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.bbva.practitioner.accounts.v1.data.DaaS;
import com.bbva.practitioner.accounts.v1.service.PropertiesService;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Ints;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import spark.Route;

@Singleton
class AccountsRestAPI implements RestAPI {

	private final String apiPort;
	private final Set<String> apiMethods;
	private final PropertiesService propertiesService;
	private final RoutesService routeService;
	private final Set<DaaS<?>> daass;
	
	@Inject
	public AccountsRestAPI(@Named("api.port") String apiPort, @Named("api.methods") String apiMethods,
							@Named("api.headers") String apiHeaders, PropertiesService propertiesService,
							RoutesService routeService, @Named("daass") Set<DaaS<?>> daass) {
		this.apiPort = apiPort;
		this.routeService = routeService;
		this.propertiesService = propertiesService;
		this.apiMethods = ImmutableSet.copyOf(Splitter.on(',').trimResults().omitEmptyStrings().split(apiMethods));
		this.daass = daass;
	}
	
	@Override
	public void start(int port) {
		
		final Integer apiPort = Ints.tryParse(this.apiPort, 10);
		
		port((apiPort == null)? port : apiPort.intValue());
		
		enableCORS("*", this.propertiesService.getProperty("api.methods"), this.propertiesService.getProperty("api.headers"));
		
		notFound("not found");
		
		internalServerError("error");
		
		final Map<String, String> endpoints = this.propertiesService.view("api.endpoint");
		
		final Splitter splitter = Splitter.on('@').limit(2).omitEmptyStrings();
		
		for( final Map.Entry<String, String> endpoint : endpoints.entrySet() ) {
			final List<String> definition = splitter.splitToList(endpoint.getValue());
			if( definition.size() != 2 ) { continue; }
			registerEndpoint(definition.get(0).toUpperCase(Locale.US), definition.get(1), endpoint.getKey());
		}
	}
	
	private void enableCORS(final String origin, final String methods, final String headers) {

	    options("/*", (request, response) -> {

	        String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
	        if (accessControlRequestHeaders != null) {
	            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
	        }

	        String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
	        if (accessControlRequestMethod != null) {
	            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
	        }

	        return "OK";
	    });

	    before((request, response) -> {
	        response.header("Access-Control-Allow-Origin", origin);
	        response.header("Access-Control-Request-Method", methods);
	        response.header("Access-Control-Allow-Headers", headers);
	    });
	}
	
	private void registerEndpoint(final String method, final String path, final String endpoint) {
		
		Preconditions.checkState(this.apiMethods.contains(method), "endpoint '%s' uses invalid method: %s", endpoint, method);
		
		final Route route = this.routeService.getRoute(endpoint);
		
		Preconditions.checkState(route != null, "endpoint '%s' ( %s::%s ) is undefined", endpoint, method, path);
		
		switch (method) {
		
			case "POST":
				post(path, route);
				break;
				
			case "PUT":
				put(path, route);
				break;
				
			case "DELETE":
				delete(path, route);
				break;
				
			case "GET":
			default:
				get(path, route);
				break;
				
		}
				
	}

	@Override
	public void terminate() {
		for(DaaS<?> daas : this.daass) {
			daas.stop();
		}
		stop();
	}

}
