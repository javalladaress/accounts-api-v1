package com.bbva.practitioner.accounts.v1.rest.post;

import java.io.IOException;

import javax.inject.Named;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountActivityService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.CreateAccountActivityResponse;

@Singleton
@Named(CreateAccountActivity.NAME)
class CreateAccountActivity extends Controller<CreateAccountActivityResponse> {

	protected static final String NAME = "createAccountActivity";

	private final AccountActivityService service;
	
	@Inject
	CreateAccountActivity(@Named("api.endpoint." + NAME) String endpoint, AccountActivityService service) {
		super(NAME, endpoint);
		this.service = service;
	}
	
	@Override
	protected void init(final HttpIO httpIO) {
		httpIO.registerType(CreateAccountActivityResponse.class);
	}
	
	@Override
	public CreateAccountActivityResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		
		final String accountId = request.pathParam("accountId");
		final String amount = request.queryParam("amount");
		
		if( Strings.isNullOrEmpty(amount) ) {
			response.status(400);
			return CreateAccountActivityResponse.ko("amount missing");
		}
		
		final double value = Double.parseDouble(amount);
		
		if( value == 0.0d ) {
			response.status(400);
			return CreateAccountActivityResponse.ko("invalid amount");
		}
		
		final AccountActivity activity = this.service.createActivity(accountId, Double.valueOf(value));
		
		response.status(200);
		return CreateAccountActivityResponse.ok("activity created", activity);
	}
	
}
