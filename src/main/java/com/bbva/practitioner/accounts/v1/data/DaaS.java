package com.bbva.practitioner.accounts.v1.data;

import javax.annotation.PostConstruct;

import com.google.common.base.Supplier;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import okhttp3.Call;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import retrofit.converter.guava.GuavaOptionalConverterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public abstract class DaaS<T> implements Supplier<T> {
	
	private final String daasEndpoint;
	
	private Retrofit retrofit;
	
	@Inject Gson gson;
	
	@Inject @Named("OkHttpClientBuilder")
	OkHttpClient.Builder httpClientBuilder;
	
	private OkHttpClient client;
	
	DaaS(final String daasEndpoint) {
		this.daasEndpoint = daasEndpoint;
	}

	@PostConstruct
	void onPostConstruct() {
		this.client = getClient(this.httpClientBuilder).build();
		this.retrofit = new Retrofit.Builder()
				.addConverterFactory(ScalarsConverterFactory.create())
				.addConverterFactory(GuavaOptionalConverterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(this.gson))
				.baseUrl(this.daasEndpoint).client(this.client).build();
	}
	
	protected T getDataAPI(final Class<T> api) {
		return this.retrofit.create(api);
	}
	
	public <X> X getAPI(final Class<X> api) {
		return this.retrofit.create(api);
	}
	
	protected abstract OkHttpClient.Builder getClient(final OkHttpClient.Builder builder);

	public void stop() {
		final Dispatcher dispatcher = this.client.dispatcher();
		for(Call call : dispatcher.queuedCalls()) {
			if( !call.isExecuted() ) { call.cancel(); }
		}
		for(Call call : dispatcher.runningCalls()) { call.cancel(); }
	}
	
}
