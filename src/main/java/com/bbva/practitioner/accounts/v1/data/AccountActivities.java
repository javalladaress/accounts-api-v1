package com.bbva.practitioner.accounts.v1.data;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;

public interface AccountActivities {
	
	@GET("collections/AccountActivity")
	Call<List<AccountActivity>> getActivity(@QueryMap Map<String, String> params);
	
	@POST("collections/AccountActivity")
	Call<AccountActivity> insertActivity(@Body AccountActivity activity);
	
	@POST("runCommand")
	Call<Map<String, Object>> runCommand(@Body RequestBody command);
	
}
