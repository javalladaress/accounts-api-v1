package com.bbva.practitioner.accounts.v1.rest;

import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;

public interface APIRouteFactory {

	public APIRoute create(Controller<? extends APIResponse> controller);
	
}
