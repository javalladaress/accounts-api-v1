package com.bbva.practitioner.accounts.v1.rest.dto.response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DeleteAccountResponse")
public class DeleteAccountResponse extends AccountsAPIResponse {

	private DeleteAccountResponse(final @Nonnull String message, final @Nullable Account account, final Boolean success) {
		super(message, account, success);
	}
	
	public static DeleteAccountResponse ok(final String message, final @Nullable Account account) {
		return new DeleteAccountResponse(message, account, Boolean.TRUE);
	}
	
	public static DeleteAccountResponse ko(final String message) {
		return new DeleteAccountResponse(message, null, Boolean.FALSE);
	}

}
