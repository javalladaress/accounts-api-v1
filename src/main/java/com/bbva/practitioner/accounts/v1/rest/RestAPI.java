package com.bbva.practitioner.accounts.v1.rest;

public interface RestAPI {

	public void start(int port);
	public void terminate();
	
}
