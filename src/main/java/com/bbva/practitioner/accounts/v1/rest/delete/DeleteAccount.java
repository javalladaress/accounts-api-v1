package com.bbva.practitioner.accounts.v1.rest.delete;

import java.io.IOException;
import java.util.Optional;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountsService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.DeleteAccountResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(DeleteAccount.NAME)
class DeleteAccount extends Controller<DeleteAccountResponse> {

	protected static final String NAME = "deleteAccount";
	
	private final AccountsService accounts;
	
	@Inject
	DeleteAccount(@Named("api.endpoint." + NAME) String endpoint, AccountsService accounts) {
		super(NAME, endpoint);
		this.accounts = accounts;
	}
	
	@Override
	protected void init(final HttpIO httpIO) {
		httpIO.registerType(DeleteAccountResponse.class);
	}
	
	@Override
	public DeleteAccountResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		
		final Optional<Account> deletedAccount = this.accounts.deleteById(request.pathParam("accountId"));
		
		if( deletedAccount.isPresent() ) {
			return DeleteAccountResponse.ok("account deleted", deletedAccount.get());
		}
		
		response.status(404);
		return DeleteAccountResponse.ko("account not found");
		
	}

}
