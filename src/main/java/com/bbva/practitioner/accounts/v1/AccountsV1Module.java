package com.bbva.practitioner.accounts.v1;

import java.util.Map;
import java.util.Properties;
import com.bbva.practitioner.accounts.v1.rest.RestModule;
import com.bbva.practitioner.accounts.v1.service.PropertiesService;
import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;

import okhttp3.OkHttpClient;

class AccountsV1Module extends AbstractModule {
	
	final Map<String, String> properties;
	
	AccountsV1Module(final Properties proeprties) {
		this.properties = propertiesToMap(proeprties);
	}
	
	@Override
	protected void configure() {
		
		bind(new TypeLiteral<Map<String, String>> () {})
		.annotatedWith(Names.named("properties"))
		.toInstance(this.properties);
		
		Names.bindProperties(binder(), this.properties);
		
		bind(PropertiesService.class).in(Scopes.SINGLETON);
		
		bind(OkHttpClient.Builder.class)
		.annotatedWith(Names.named("OkHttpClientBuilder"))
		.toInstance(new OkHttpClient.Builder());
		
		install(new RestModule());
		
	}
	
	private Map<String, String> propertiesToMap(final Properties properties) {
		
		final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
		
		for (final String property : properties.stringPropertyNames()) {
			builder.put(property, properties.getProperty(property, ""));
		}
		
		return builder.build();
	}

}
