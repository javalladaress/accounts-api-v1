package com.bbva.practitioner.accounts.v1.data;

public interface DaaSBodyProvider<T> {
	
	public T getDaaSBody();

}
