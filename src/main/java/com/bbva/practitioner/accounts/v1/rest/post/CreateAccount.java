package com.bbva.practitioner.accounts.v1.rest.post;

import java.io.IOException;
import java.util.Optional;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountsService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.request.CreateAccountRequest;
import com.bbva.practitioner.accounts.v1.rest.dto.response.CreateAccountResponse;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(CreateAccount.NAME)
class CreateAccount extends Controller<CreateAccountResponse> {

	protected static final String NAME = "createAccount";

	private final AccountsService accounts;
	
	@Inject
	CreateAccount(@Named("api.endpoint." + NAME) String endpoint, AccountsService accounts) {
		super(NAME, endpoint);
		this.accounts = accounts;
	}
	
	@Override
	protected void init(final HttpIO httpIO) {
		httpIO.registerType(CreateAccountResponse.class);
	}
	
	@Override
	public CreateAccountResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		
		final CreateAccountRequest createAccountRequest = request.body(CreateAccountRequest.class);
		
		Account account = createAccountRequest.get();
		
		if( !isValid(account) ) {
			response.status(400);
			return CreateAccountResponse.ko("invalid account");
		}
		
		final Optional<Account> deletedAccount = this.accounts.create(account);
		
		if( !deletedAccount.isPresent() ) {
			response.status(200);
			return CreateAccountResponse.ko("created");
		}
		
		account = deletedAccount.get();
		
		if( account.hasEntityInfo() ) {
			response.status(201);
			return CreateAccountResponse.ok("created", account);
		}
		
		response.status(409);
		return CreateAccountResponse.ko("not created");
		
	}

	private boolean isValid(final Account account) {
		final String accountId = account.getId();
		return !( Strings.isNullOrEmpty(accountId) || (accountId.length() < 8) );
	}
	
}
