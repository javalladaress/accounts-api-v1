package com.bbva.practitioner.accounts.v1.rest.dto.response;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;
import com.google.common.base.MoreObjects;
import com.google.common.base.Supplier;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public abstract class AccountsActivityAPIResponse extends APIResponse implements Supplier<Collection<AccountActivity>> {
	
	@Expose
	@Since(1.0)
	@SerializedName("activity")
	@XStreamAlias("activity")
	private Collection<AccountActivity> activity;
	
	protected AccountsActivityAPIResponse(final @Nonnull String message, 
			final @Nullable Collection<AccountActivity> activity, final @Nonnull Boolean success) {
		super(success, message);
		this.activity = activity;
	}
	
	@Override
	public Collection<AccountActivity> get() {
		return this.activity;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("activity", get()).toString();
	}
	
}
