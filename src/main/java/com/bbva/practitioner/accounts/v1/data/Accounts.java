package com.bbva.practitioner.accounts.v1.data;

import java.util.List;
import java.util.Map;

import com.bbva.practitioner.accounts.v1.data.dto.Account;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface Accounts {
	
	@GET("collections/Accounts")
	Call<List<Account>> getAccounts();
	
	@GET("collections/Accounts")
	Call<Integer> countAccounts(@QueryMap Map<String, String> params);
	
	@GET("collections/Accounts")
	Call<List<Account>> getAccount(@QueryMap Map<String, String> params);
	
	@GET("collections/Accounts")
	Call<List<Account>> getAccount(@Query("q") String q);
	
	@POST("collections/Accounts")
	Call<Account> createAccount(@Body Account account);
	
	@PUT("collections/Accounts")
	Call<Map<String, Object>> updateAccount(@Query("q") String q, @Body Map<String, Object> body);
	
	@DELETE("collections/Accounts/{id}")
	Call<Account> deleteAccount(@Path("id") String id);
	
}
