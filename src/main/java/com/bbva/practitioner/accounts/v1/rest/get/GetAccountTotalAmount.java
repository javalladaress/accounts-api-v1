package com.bbva.practitioner.accounts.v1.rest.get;

import java.io.IOException;
import java.util.Map;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountActivityService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountTotalAmountResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(GetAccountTotalAmount.NAME)
class GetAccountTotalAmount extends Controller<GetAccountTotalAmountResponse> {

	protected static final String NAME = "getAccountTotalAmount";

	private final AccountActivityService activity;
	
	@Inject
	GetAccountTotalAmount(@Named("api.endpoint." + NAME) String endpoint, AccountActivityService activity) {
		super(NAME, endpoint);
		this.activity = activity;
	}
	
	@Override
	protected void init(HttpIO httpIO) {
		httpIO.registerType(GetAccountTotalAmountResponse.class);
	}
	
	@Override
	public GetAccountTotalAmountResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		final Map<String, Object> totalAmount = this.activity.sumAmountsByAccount(request.pathParam("accountId"));
		return GetAccountTotalAmountResponse.ok(totalAmount);
	}

}
