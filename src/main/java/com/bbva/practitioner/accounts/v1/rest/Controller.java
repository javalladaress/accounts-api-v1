package com.bbva.practitioner.accounts.v1.rest;

import java.io.IOException;

import javax.annotation.PostConstruct;

import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.base.Supplier;
import com.google.common.primitives.Longs;
import com.google.inject.Inject;

public abstract class Controller<T extends APIResponse> implements Supplier<String> {
	
	
	private final String name;
	private final String endpoint;
	
	@Inject
	private HttpIO httpIO;
	
	protected Controller(final String name, final String endpoint) {
		this.name = name;
		this.endpoint = endpoint;
	}
	
	@PostConstruct
	void onPostConstruct() {
		init(this.httpIO);
	}
	
	protected abstract void init(final HttpIO restIO);
	
	public abstract T process(final HttpRequest request, final HttpResponse response) throws IOException;
	
	protected String toJson(final Object source) {
		return this.httpIO.get().toJson(source);
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).addValue(this.getClass().getName()).add(this.endpoint, this.name).toString();
	}

	public String get() {
		return this.name + "@" + this.endpoint;
	}
	
	protected long getLongParam(final HttpRequest request, final String param, long defaultValue) {
		final Long value = Longs.tryParse(Strings.nullToEmpty(request.queryParam(param)));
		return ( value == null )? defaultValue : value.longValue();
	}
	
}
