package com.bbva.practitioner.accounts.v1;

import java.io.IOException;
import java.util.Properties;

import com.bbva.practitioner.accounts.v1.rest.RestAPI;
import com.google.common.base.Strings;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.netflix.governator.guice.LifecycleInjector;
import com.netflix.governator.lifecycle.LifecycleManager;

public class AccountsV1 {

	private final Stage stage;
	private final Injector injector;
	
	private final Properties properties;

	private AccountsV1(final Properties properties, final String environment) {
		this.properties = properties;
		
		final String env = Strings.isNullOrEmpty(environment)? this.properties.getProperty("api.env", "dev") : environment;
		
		this.stage = (env.equalsIgnoreCase("pro")) ? Stage.PRODUCTION : Stage.DEVELOPMENT;
		
		this.injector = LifecycleInjector.builder()
				.withModules(new AccountsV1Module(this.properties))
				.inStage(this.stage).build().createInjector();	
	}

	private LifecycleManager getLifecycleManager() {
		return this.injector.getInstance(LifecycleManager.class);
	}

	private void start(final int port) {
		try {
			getLifecycleManager().start();
			this.injector.getInstance(RestAPI.class).start(port);
		} catch (final Exception e) {
			e.printStackTrace(System.err);
		}
	}
	
	private void stop() {
		try {
			this.injector.getInstance(RestAPI.class).terminate();
			getLifecycleManager().close();
		} catch (final Exception e) {
			e.printStackTrace(System.err);
		}
	}

	public static void main(final String[] args) {
		
		final Properties properties = new Properties();
		
		try {
			properties.load(AccountsV1.class.getResourceAsStream("/api.properties"));
		} catch (final IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
		
		final AccountsV1 api = new AccountsV1(properties, getProperty("api.env", "dev"));
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override public void run() { api.stop(); }
		});
		
		api.start(getIntProperty("api.port"));
		
	}

	private static String getProperty(final String propertyName, final String defVal) {
		return Strings.nullToEmpty(System.getProperty(propertyName, defVal));
	}

	private static int getIntProperty(final String propertyName) {
		return Integer.valueOf(getProperty(propertyName, "13001"), 10);
	}
}
