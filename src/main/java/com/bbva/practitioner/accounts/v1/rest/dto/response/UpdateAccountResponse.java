package com.bbva.practitioner.accounts.v1.rest.dto.response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UpdateAccountResponse")
public class UpdateAccountResponse extends AccountsAPIResponse {

	@Expose
	@Since(1.0)
	@SerializedName("count")
	@XStreamAlias("count")
	private final int count;
	
	private UpdateAccountResponse(final @Nonnull String message, final @Nullable Account account, final int count, final Boolean success) {
		super(message, account, success);
		this.count = count;
	}
	
	public static UpdateAccountResponse ok(final String message, final Account account, final int count) {
		return new UpdateAccountResponse(message, account, count, Boolean.TRUE);
	}
	
	public static UpdateAccountResponse ko(final String message) {
		return new UpdateAccountResponse(message, null, 0, Boolean.FALSE);
	}

}
