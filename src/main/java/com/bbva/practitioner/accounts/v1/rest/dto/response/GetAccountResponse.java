package com.bbva.practitioner.accounts.v1.rest.dto.response;

import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("GetAccountResponse")
public class GetAccountResponse extends AccountsAPIResponse {
	
	private GetAccountResponse(final String message, final @Nullable Account account, final Boolean success) {
		super(message, account, success);
	}
	
	public static GetAccountResponse ok(final @Nullable Account account) {
		return new GetAccountResponse("not found", account, Boolean.TRUE);
	}
	
	public static GetAccountResponse ko() {
		return new GetAccountResponse("not found", null, Boolean.FALSE);
	}
	
}
