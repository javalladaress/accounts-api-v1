package com.bbva.practitioner.accounts.v1.data.dto;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("account")
public class Account {
	
	@Expose
	@Since(1.0)
	@SerializedName("accountId")
	@XStreamAlias("accountId")
	private String id;
	
	@Expose
	@Since(1.0)
	@SerializedName(value = "description", alternate = "desc")
	@XStreamAlias("description")
	private String description;
	
	@Since(1.0)
	@Expose(deserialize = true, serialize = false)
	@SerializedName(value = "id", alternate = { "_id" })
	@XStreamOmitField
	private MLabId mLabId;
	
	private Account() { }
	
	public String getId() {
		return this.id;
	}
	
	public String getDescription() {
		return this.description;
	}

	public String getHash() {
		return Hashing.crc32c().newHasher()
				.putString(this.id, Charsets.UTF_8)
				.putString(this.description, Charsets.UTF_8)
				.hash().toString();
	}
	
	public boolean hasEntityInfo() {
		return this.mLabId != null;
	}
	
	public String getEntityId() {
		return ( this.mLabId == null )? null : this.mLabId.getId();
	}
	
}
