package com.bbva.practitioner.accounts.v1.rest.put;

import java.io.IOException;
import java.util.Optional;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountsService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.request.UpdateAccountRequest;
import com.bbva.practitioner.accounts.v1.rest.dto.response.UpdateAccountResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(UpdateAccount.NAME)
class UpdateAccount extends Controller<UpdateAccountResponse> {

	protected static final String NAME = "updateAccount";

	private final AccountsService accounts;
	
	@Inject
	UpdateAccount(@Named("api.endpoint." + NAME) String endpoint, AccountsService accounts) {
		super(NAME, endpoint);
		this.accounts = accounts;
	}
	
	@Override
	protected void init(final HttpIO httpIO) {
		httpIO.registerType(UpdateAccountResponse.class);
	}
	
	@Override
	public UpdateAccountResponse process(final HttpRequest request, final HttpResponse response) throws IOException {

		final String accountId = request.pathParam("accountId");
		
		final UpdateAccountRequest updateAccountRequest = request.body(UpdateAccountRequest.class);
		
		final Account account = updateAccountRequest.get();
		final int count = this.accounts.updateById(accountId, account);
		
		if( count < 0 ) {
			response.status(404);
			return UpdateAccountResponse.ko("account not found");
		}
		
		if( count == 0 ) {
			response.status(205);
			return UpdateAccountResponse.ko("account not updated");
		}
		
		final Optional<Account> updatedAccount = this.accounts.getById(accountId);
		
		if( updatedAccount.isPresent() ) {
			response.status(200);
			return UpdateAccountResponse.ok("account updated", updatedAccount.get(), count);
		}
		
		response.status(204);
		return UpdateAccountResponse.ok("account updated", account, count);
	}

}
