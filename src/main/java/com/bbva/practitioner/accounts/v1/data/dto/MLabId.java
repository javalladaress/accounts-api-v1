package com.bbva.practitioner.accounts.v1.data.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

class MLabId {
	
	@Since(1.0)
	@Expose(deserialize = true, serialize = false)
	@SerializedName(value = "$oid")
	@XStreamOmitField
	private String id;
	
	MLabId() {}
	
	public String getId() {
		return this.id;
	}

}
