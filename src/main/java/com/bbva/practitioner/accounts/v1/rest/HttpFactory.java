package com.bbva.practitioner.accounts.v1.rest;

import spark.Request;
import spark.Response;

interface HttpFactory {

	public HttpRequest getAPIRequest(Request request);
	public HttpResponse getAPIResponse(Response response);
	
}
