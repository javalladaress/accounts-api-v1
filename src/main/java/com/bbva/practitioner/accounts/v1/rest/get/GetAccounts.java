package com.bbva.practitioner.accounts.v1.rest.get;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountsService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(GetAccounts.NAME)
class GetAccounts extends Controller<GetAccountsResponse> {

	protected static final String NAME = "getAccounts";
	
	private final AccountsService accounts;
	
	@Inject
	GetAccounts(@Named("api.endpoint." + NAME) String endpoint, AccountsService accounts) {
		super(NAME, endpoint);
		this.accounts = accounts;
	}
	
	@Override
	protected void init(final HttpIO httpIO) {
		httpIO.registerType(GetAccountsResponse.class);
	}
	
	@Override
	public GetAccountsResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		
		final List<Account> accounts = this.accounts.getAll();
		
		if( ( accounts == null ) || ( accounts.isEmpty() ) ) {
			response.status(204);
			return GetAccountsResponse.ok(Collections.emptyList());
		}
		
		response.status(200);
		return GetAccountsResponse.ok(accounts);
	}

}
