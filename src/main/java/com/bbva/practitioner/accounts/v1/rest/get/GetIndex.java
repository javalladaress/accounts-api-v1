package com.bbva.practitioner.accounts.v1.rest.get;

import java.io.IOException;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetIndexResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(GetIndex.NAME)
class GetIndex extends Controller<GetIndexResponse> {
	
	static final String NAME = "getIndex";
	
	@Inject
	public GetIndex(HttpIO restIO, @Named("api.endpoint." + NAME) String endpoint) {
		super(NAME, endpoint);
	}
	
	@Override
	protected void init(final HttpIO httpIO) {
		httpIO.registerType(GetIndexResponse.class);
	}
	
	@Override
	public GetIndexResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		response.status(200);
		return new GetIndexResponse("accounts", 1.0d);
	}
	
}
