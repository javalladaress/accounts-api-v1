package com.bbva.practitioner.accounts.v1.rest.post;

import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.CreateAccountActivityResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.CreateAccountResponse;
import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class PostRoutesModule extends AbstractModule {
	
	final MapBinder<String, Controller<? extends APIResponse>> controllers;
	
	public PostRoutesModule(final MapBinder<String, Controller<? extends APIResponse>> controllers) {
		this.controllers = controllers;
	}
	
	@Override
	protected void configure() {
		
		final TypeLiteral<Controller<CreateAccountResponse>> createAccount = new TypeLiteral<Controller<CreateAccountResponse>>() {};
		final TypeLiteral<Controller<CreateAccountActivityResponse>> createAccountActivity = new TypeLiteral<Controller<CreateAccountActivityResponse>>() {};
		
		final Named createAccountName = Names.named(CreateAccount.NAME);
		final Named createAccountActivityName = Names.named(CreateAccountActivity.NAME);
		
		bind(createAccount).annotatedWith(createAccountName).to(CreateAccount.class).in(Scopes.SINGLETON);
		bind(createAccountActivity).annotatedWith(createAccountActivityName).to(CreateAccountActivity.class).in(Scopes.SINGLETON);
		
		this.controllers.addBinding(CreateAccount.NAME).to(Key.get(createAccount, createAccountName)).in(Scopes.SINGLETON);
		this.controllers.addBinding(CreateAccountActivity.NAME).to(Key.get(createAccountActivity, createAccountActivityName)).in(Scopes.SINGLETON);
		
	}

}
