package com.bbva.practitioner.accounts.v1.service;

import java.util.Map;

import javax.inject.Named;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class PropertiesService {

	private final Map<String, String> properties;
	
	@Inject
	public PropertiesService(@Named("properties") Map<String, String> properties) {
		this.properties = properties;
	}
	
	public String getProperty(final String key) {
		return this.properties.get(key);
	}
	
	public String getProperty(final String key, String defVal) {
		final String value = getProperty(key);
		return Strings.isNullOrEmpty(value)? defVal : value;
	}
	
	public Map<String, String> view(final String scope) {
		
		final String prefix = scope + ".";
		final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
		
		for( final Map.Entry<String, String> property : this.properties.entrySet()) {
			final String key = property.getKey();
			if( key.startsWith(prefix) ) {
				builder.put(key.substring(prefix.length()), property.getValue());
			}
		}
		
		return builder.build();
	}
	
}
