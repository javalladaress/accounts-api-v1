package com.bbva.practitioner.accounts.v1.data.dto;

import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("account-activity")
public class AccountActivity {
	
	@Expose
	@Since(1.0)
	@SerializedName("account")
	@XStreamAlias("account")
	private String account;
	
	@Expose
	@Since(1.0)
	@SerializedName(value = "amount")
	@XStreamAlias("amount")
	private Double amount;
	
	@Expose
	@Since(1.0)
	@SerializedName(value = "rates")
	private Map<String, Double> rates;
	
	@Expose
	@Since(1.0)
	@SerializedName(value = "timestamp")
	@XStreamAlias("timestamp")
	private Long timestamp;
	
	@Since(1.0)
	@Expose(deserialize = true, serialize = false)
	@SerializedName(value = "id", alternate = { "_id" })
	@XStreamOmitField
	private MLabId mLabId;
	
	public AccountActivity() { }
	
	public String getAccount() {
		return this.account;
	}
	
	public void setAccount(final String account) {
		this.account = account;
	}
	
	public Double getAmmount() {
		return this.amount;
	}
	
	public void setAmmount(final double amount) {
		this.amount = Double.valueOf(amount);
	}
	
	public Map<String, Double> getRates() {
		return this.rates;
	}
	
	public void setRates(final Map<String, Double> rates) {
		this.rates = rates;
	}

	public Long getTimestamp() {
		return this.timestamp;
	}
	
	public void setTimestamp(final long timestamp) {
		this.timestamp = Long.valueOf(timestamp);
	}
	
	public String getHash() {
		return Hashing.crc32c().newHasher()
				.putString(this.account, Charsets.UTF_8)
				.putDouble(this.amount.doubleValue())
				.putLong(this.timestamp.longValue())
				.hash().toString();
	}
	
	public boolean hasEntityInfo() {
		return this.mLabId != null;
	}
	
	public String getEntityId() {
		return ( this.mLabId == null )? null : this.mLabId.getId();
	}
	
}
