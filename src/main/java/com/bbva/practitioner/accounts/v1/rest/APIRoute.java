package com.bbva.practitioner.accounts.v1.rest;

import java.io.IOException;

import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.google.common.base.Strings;
import com.google.common.net.HttpHeaders;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;

import spark.Request;
import spark.Response;
import spark.Route;

public class APIRoute implements Route {

	private final HttpIO httpIO;
	private final HttpFactory apiIOFactory;
	private final Controller<? extends APIResponse> controller;
	
	@AssistedInject
	APIRoute(HttpIO restIO, HttpFactory apiIOFactory,
				@Assisted Controller<? extends APIResponse> controller) {
		this.httpIO = restIO;
		this.apiIOFactory = apiIOFactory;
		this.controller = controller;
	}
	
	@Override
	public Object handle(Request request, Response response) {
		
		final APIResponse apiResponse;
		
		try {
			apiResponse = this.controller.process(toHttpRequest(request), toHttpResponse(response));
		} catch (IOException e) {
			e.printStackTrace();
			response.status(500);
			return response.raw();
		}
		
		switch( getResponseFormat(request) ) {
		
			case 1:
				this.httpIO.toXml(response, apiResponse);
				break;
			
			case 0:
			default:
				this.httpIO.toJson(response, apiResponse);
				break;
				
		}
		
		return response.raw();
	}
	
	private HttpRequest toHttpRequest(final Request request) {
		return this.apiIOFactory.getAPIRequest(request);
	}
	
	private HttpResponse toHttpResponse(final Response response) {
		return this.apiIOFactory.getAPIResponse(response);
	}
	
	private int getResponseFormat(final Request request) {
		
		String format = request.headers(HttpHeaders.ACCEPT);
		
		if( !Strings.isNullOrEmpty(format) ) {
			return parseFormat(format);
		}
		
		format = request.queryParams("alt");
		
		return Strings.isNullOrEmpty(format)? 0 : parseFormat(format);
	}
	
	private int parseFormat(final String format) {
		return format.contains("/xml")? 1 : 0;
	}
	
}
