package com.bbva.practitioner.accounts.v1.rest.dto.response;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("response")
public class GetIndexResponse extends APIResponse {
	
	@Expose
	@Since(1.0)
	@SerializedName("name") 
	@XStreamAlias("name")
	private final String name;
	
	@Expose
	@Since(1.0)
	@SerializedName("version") 
	@XStreamAlias("version")
	private final double version;
	
	public GetIndexResponse() {
		this("?", 0.0);
	}
	
	public GetIndexResponse(final String name, final double version) {
		super(Boolean.TRUE, name + "@" + version);
		this.name = name;
		this.version = version;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("name", this.name)
				.add("version", this.version)
				.toString();
	}
	
}
