package com.bbva.practitioner.accounts.v1.rest.dto.response;

import java.util.List;

import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("GetAccountActivityResponse")
public class GetAccountActivityResponse extends AccountsActivityAPIResponse {
	
	private GetAccountActivityResponse(final String message, final @Nullable List<AccountActivity> activitiy, final Boolean success) {
		super(message, activitiy, success);
	}
	
	public static GetAccountActivityResponse ok(final @Nullable List<AccountActivity> activitiy) {
		return new GetAccountActivityResponse("found", activitiy, Boolean.TRUE);
	}
	
	public static GetAccountActivityResponse ko() {
		return new GetAccountActivityResponse("not found", null, Boolean.FALSE);
	}
	
}
