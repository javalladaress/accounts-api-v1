package com.bbva.practitioner.accounts.v1.rest;

import com.bbva.practitioner.accounts.v1.data.DaaSModule;
import com.bbva.practitioner.accounts.v1.rest.delete.DeleteRoutesModule;
import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.bbva.practitioner.accounts.v1.rest.get.GetRoutesModule;
import com.bbva.practitioner.accounts.v1.rest.post.PostRoutesModule;
import com.bbva.practitioner.accounts.v1.rest.put.PutRoutesModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import com.thoughtworks.xstream.XStream;

import spark.Route;

public class RestModule extends AbstractModule {

	@Override
	protected void configure() {
		
		install(new FactoryModuleBuilder()
				.implement(Route.class, APIRoute.class)
				.build(APIRouteFactory.class));
		
		install(new FactoryModuleBuilder().build(HttpFactory.class));
		
		bind(HttpIO.class).in(Scopes.SINGLETON);
		
		install(new DaaSModule());
		
		final MapBinder<String, Controller<? extends APIResponse>> controllersBinder = MapBinder.newMapBinder(binder(), 
				new TypeLiteral<String>() {}, new TypeLiteral<Controller<? extends APIResponse>>() {}, Names.named("controllers"));
		
		install(new GetRoutesModule(controllersBinder));
		install(new PostRoutesModule(controllersBinder));
		install(new PutRoutesModule(controllersBinder));
		install(new DeleteRoutesModule(controllersBinder));
		
		bind(AccountsService.class).in(Scopes.SINGLETON);
		bind(AccountActivityService.class).in(Scopes.SINGLETON);
		bind(RoutesService.class).in(Scopes.SINGLETON);
		
		bind(RestAPI.class).to(AccountsRestAPI.class).asEagerSingleton();
		
	}

	@Provides
	@Singleton
	Gson provideGson() {
		return new GsonBuilder().disableHtmlEscaping()
				.excludeFieldsWithoutExposeAnnotation()
				.setVersion(1.0).create();
	}
	
	@Provides
	@Singleton
	XStream provideXStream() {
		return new XStream();
	}

}
