package com.bbva.practitioner.accounts.v1.rest.dto.response;

import java.util.Collection;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("GetAccountsResponse")
public class GetAccountsResponse extends AccountsAPIResponse {
	
	public GetAccountsResponse(final String message, final Collection<Account> accounts, final Boolean success) {
		super(message, accounts, success);
	}
	
	public static GetAccountsResponse ok(final Collection<Account> accounts) {
		return new GetAccountsResponse("accounts", accounts, Boolean.TRUE);
	}
	
	public static GetAccountsResponse ko() {
		return new GetAccountsResponse("error", null, Boolean.FALSE);
	}
	
}
