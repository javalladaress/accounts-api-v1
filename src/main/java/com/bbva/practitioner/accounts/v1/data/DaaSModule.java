package com.bbva.practitioner.accounts.v1.data;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class DaaSModule extends AbstractModule {

	@Override
	protected void configure() {

		bind(new TypeLiteral<DaaS<Accounts>>() {})
		.annotatedWith(Names.named("accountsDaaS"))
		.to(AccountsDaaS.class).in(Scopes.SINGLETON);
		
		final Multibinder<DaaS<?>> daasBinder = Multibinder.newSetBinder(binder(), new TypeLiteral<DaaS<?>>() {}, Names.named("daass"));
		daasBinder.addBinding().to(Key.get(new TypeLiteral<DaaS<Accounts>>() {}, Names.named("accountsDaaS")));

	}

	@Provides @Singleton @Named("accountsAPI")
	Accounts provideAccountsDaaS(@Named("accountsDaaS") DaaS<Accounts> daas) {
		return daas.get();
	}
	
	@Provides @Singleton @Named("accountActivityAPI")
	AccountActivities provideAccountActivityDaaS(@Named("accountsDaaS") DaaS<Accounts> daas) {
		return daas.getAPI(AccountActivities.class);
	}

}
