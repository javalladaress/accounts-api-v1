package com.bbva.practitioner.accounts.v1.rest.put;

import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.UpdateAccountResponse;
import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class PutRoutesModule extends AbstractModule {
	
	final MapBinder<String, Controller<? extends APIResponse>> controllers;
	
	public PutRoutesModule(final MapBinder<String, Controller<? extends APIResponse>> controllers) {
		this.controllers = controllers;
	}
	
	@Override
	protected void configure() {
		
		final TypeLiteral<Controller<UpdateAccountResponse>> updateAccount = new TypeLiteral<Controller<UpdateAccountResponse>>() {};
		final Named updateAccountName = Names.named(UpdateAccount.NAME);
		
		bind(updateAccount).annotatedWith(updateAccountName).to(UpdateAccount.class).in(Scopes.SINGLETON);
		
		this.controllers.addBinding(UpdateAccount.NAME).to(Key.get(updateAccount, updateAccountName)).in(Scopes.SINGLETON);
		
	}

}
