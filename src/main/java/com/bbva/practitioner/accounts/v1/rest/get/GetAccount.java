package com.bbva.practitioner.accounts.v1.rest.get;

import java.io.IOException;
import java.util.Optional;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.bbva.practitioner.accounts.v1.rest.HttpRequest;
import com.bbva.practitioner.accounts.v1.rest.HttpResponse;
import com.bbva.practitioner.accounts.v1.rest.AccountsService;
import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.HttpIO;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Named(GetAccount.NAME)
class GetAccount extends Controller<GetAccountResponse> {

	protected static final String NAME = "getAccount";

	private final AccountsService accounts;
	
	@Inject
	GetAccount(@Named("api.endpoint." + NAME) String endpoint, AccountsService accounts) {
		super(NAME, endpoint);
		this.accounts = accounts;
	}
	
	@Override
	protected void init(HttpIO httpIO) {
		httpIO.registerType(GetAccountResponse.class);
	}
	
	@Override
	public GetAccountResponse process(final HttpRequest request, final HttpResponse response) throws IOException {
		
		final Optional<Account> account = this.accounts.getById(request.pathParam("accountId"));

		if( account.isPresent() ) {
			response.status(200);
			return GetAccountResponse.ok(account.get());
		}
		
		response.status(404);
		return GetAccountResponse.ko();
		
	}

}
