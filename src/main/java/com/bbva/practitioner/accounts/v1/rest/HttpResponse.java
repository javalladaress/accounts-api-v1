package com.bbva.practitioner.accounts.v1.rest;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;

import spark.Response;

public class HttpResponse {

	private final Response response;
	
	@AssistedInject
	HttpResponse(Gson gson, @Assisted Response response) {
		this.response = response;
	}
	
	public HttpResponse status(final int code) {
		Preconditions.checkArgument( (code >= 200) && (code <= 599) );
		this.response.status(code);
		return this;
	}
	
}
