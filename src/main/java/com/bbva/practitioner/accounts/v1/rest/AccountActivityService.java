package com.bbva.practitioner.accounts.v1.rest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.AccountActivities;
import com.bbva.practitioner.accounts.v1.data.dto.AccountActivity;
import com.bbva.practitioner.accounts.v1.service.PropertiesService;
import com.google.common.base.Strings;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

@Singleton
public class AccountActivityService implements Supplier<Map<String, Double>> {
	
	private final Gson gson;
	private final AccountActivities api;
	
	@Inject
	private PropertiesService properties;
	
	private Map<String, String> accountActivityQueries;
	
	private Supplier<Map<String, Double>> ratesSupplier;
	
	@Inject
	public AccountActivityService(Gson gson, @Named("accountActivityAPI") AccountActivities activity) {
		this.gson = gson;
		this.api = activity;
	}
	
	@PostConstruct
	void onPostConstruct() {
		this.accountActivityQueries = this.properties.view("daas.queries.accountActivity");
		this.ratesSupplier = Suppliers.memoizeWithExpiration(this, 10L, TimeUnit.MINUTES);
	} 
	
	private String queryByAccountId(final String accountId) {
		return this.gson.toJson(ImmutableMap.of("account", accountId));
	}
	
	private String getSort(final String field, final int direction) {
		return this.gson.toJson(ImmutableMap.of(field, direction));
	}
	
	public List<AccountActivity> getLastActivity(final String accountId) throws IOException {
		return getActivity(accountId, 0l, 1l);
	}
	
	public List<AccountActivity> getActivity(final String accountId, final long firstIndex, final long partitionSize) throws IOException {
		
		final Response<List<AccountActivity>> response = this.api.getActivity(
				ImmutableMap.of("q", queryByAccountId(accountId), "s", getSort("timestamp", -1), 
						"l", Long.toString(partitionSize, 10), "sk", Long.toString(firstIndex, 10))).execute();
		
		if( response.isSuccessful() ) {
			return response.body();
		}
		
		throw new IOException(response.message());
	}
	
	public AccountActivity createActivity(final String accountId, final Double amount) throws IOException {
		
		final AccountActivity activity = new AccountActivity();
		
		activity.setAccount(accountId);
		activity.setAmmount(amount);
		activity.setTimestamp(System.currentTimeMillis());
		activity.setRates(this.ratesSupplier.get());
		
		final Response<AccountActivity> response = this.api.insertActivity(activity).execute();
		
		if( response.isSuccessful() ) {
			return response.body();
		}
		
		throw new IOException();
	}
	
	public Map<String, Object> sumAmountsByAccount(final String accountId) throws IOException {
		final String command = String.format(Strings.nullToEmpty(this.accountActivityQueries.get("sumAmountsByAccount")), accountId);
		final RequestBody body = RequestBody.create(command, MediaType.parse(com.google.common.net.MediaType.JSON_UTF_8.toString()));
		return this.api.runCommand(body).execute().body();
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Double> getExchangeRates() throws IOException {
		final URL url = new URL(this.properties.getProperty("api.bmx"));
		final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setRequestMethod("GET");
		connection.setInstanceFollowRedirects(false);
		connection.setRequestProperty("Accept", com.google.common.net.MediaType.JSON_UTF_8.toString());
		connection.setRequestProperty("Bmx-Token", Strings.nullToEmpty(this.properties.getProperty("api.bmx.token")));
		
		final Map<String, Object> bmxResponse = this.gson.fromJson(
				new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8), 
				(new TypeToken<Map<String, Object>>(){}).getType());
		
		final List<Map<String, Object>> series = (List<Map<String, Object>>) ((Map<String, Object>) bmxResponse.get("bmx")).get("series");
		
		final ImmutableMap.Builder<String, Double> rates = ImmutableMap.builder();
		
		for( final Map<String, Object> serie : series ) {
			rates.put(bmxToISO((String) serie.get("idSerie")), getExchangeRate((List<Map<String, String>>) serie.get("datos")));
		}
		
		return rates.build();
	}
	
	private String bmxToISO(final String bmx) {
		switch( bmx ) {
			case "SF43718":
				return "USD";
			case "SF46410":
				return "EUR";
			default: /* NO_GO */
		}
		return "XXX";
	}
	
	private Double getExchangeRate(final List<Map<String, String>> input) {
		if( (input == null) || input.isEmpty() ) { return Double.valueOf(0.0d); }
		return Double.valueOf(Double.parseDouble(input.get(0).get("dato")));
	}

	@Override
	public Map<String, Double> get() {
		try {
			return getExchangeRates();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Collections.emptyMap();
	}

}
