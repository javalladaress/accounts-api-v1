package com.bbva.practitioner.accounts.v1.rest;

import java.util.Map;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.google.common.base.MoreObjects;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import spark.Route;

@Singleton
class RoutesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoutesService.class);
	
	private final Map<String, Controller<? extends APIResponse>> controllers;
	private final APIRouteFactory apiRouteFactory;
	
	@Inject
	public RoutesService(@Named("controllers") Map<String, Controller<? extends APIResponse>> controllers, APIRouteFactory apiRouteFactory) {
		this.controllers = controllers;
		this.apiRouteFactory = apiRouteFactory;
	}
	
	@Nullable
	public Route getRoute(final String endpoint) {
		if( !this.controllers.containsKey(endpoint) ) { return null; }
		final Controller<? extends APIResponse> controller = this.controllers.get(endpoint);
		LOGGER.info(MoreObjects.toStringHelper(this).addValue(controller).toString());
		return this.apiRouteFactory.create(controller);
	}
	
}
