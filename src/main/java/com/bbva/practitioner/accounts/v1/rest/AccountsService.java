package com.bbva.practitioner.accounts.v1.rest;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Named;

import com.bbva.practitioner.accounts.v1.data.Accounts;
import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import retrofit2.Response;

@Singleton
public class AccountsService {
	
	private final Gson gson;
	private final Accounts accounts;
	
	@Inject
	public AccountsService(Gson gson, @Named("accountsAPI") Accounts accounts) {
		this.gson = gson;
		this.accounts = accounts;
	}
	
	private String queryById(final String accountId) {
		return this.gson.toJson(ImmutableMap.of("accountId", accountId));
	}
	
	public boolean existsById(final String accountId) throws IOException {

		final Response<Integer> response = this.accounts.countAccounts(ImmutableMap.of("q", queryById(accountId), "c", "true")).execute();
		
		if( response.isSuccessful() ) {
			return response.body().intValue() > 0;
		}
		
		throw new IOException(response.message());
	}
	
	public Optional<Account> getById(final String accountId) throws IOException {
		
		final Response<List<Account>> response = this.accounts.getAccount(ImmutableMap.of("q", queryById(accountId))).execute();
		
		if( response.isSuccessful() ) {
			return Optional.ofNullable(Iterables.getFirst(response.body(), null));
		}
		
		throw new IOException(response.message());
	}
	
	public Optional<Account> deleteById(final String accountId) throws IOException {
		
		final Optional<Account> account = getById(accountId);
		
		if( !account.isPresent() ) {
			return Optional.empty();
		}

		final retrofit2.Response<Account> response = this.accounts.deleteAccount(account.get().getEntityId()).execute();
		
		if( response.isSuccessful() ) {
			return account;
		}
		
		throw new IOException(response.message());
	}
	
	public List<Account> getAll() throws IOException {
		
		final retrofit2.Response<List<Account>> response = this.accounts.getAccounts().execute();
		
		if( response.isSuccessful() ) {
			return response.body();
		}
		
		throw new IOException(response.message());
	}
	
	public Optional<Account> create(final Account account) throws IOException {
		
		final boolean exists = existsById(account.getId());
		
		if( exists ) {
			return Optional.of(account);
		}
		
		final retrofit2.Response<Account> response = this.accounts.createAccount(account).execute();
		
		if( response.isSuccessful() ) {
			return Optional.ofNullable(response.body());
		}
		
		throw new IOException(response.message());
	}
	
	public int updateById(final String accountId, final Account account) throws IOException {
		
		final boolean exists = existsById(accountId);
		
		if( !exists ) { return -1; }
		
		final retrofit2.Response<Map<String, Object>> response = 
				this.accounts.updateAccount(queryById(accountId), ImmutableMap.of("$set", account)).execute();
		
		if( !response.isSuccessful() ) {
			throw new IOException(response.message());
		}
		
		final Map<String, Object> data = response.body();
		final Double count = (Double) data.get("n");
		
		return count.intValue();
	}

}
