package com.bbva.practitioner.accounts.v1.rest.delete;

import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.DeleteAccountResponse;
import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class DeleteRoutesModule extends AbstractModule {
	
	final MapBinder<String, Controller<? extends APIResponse>> controllers;
	
	public DeleteRoutesModule(final MapBinder<String, Controller<? extends APIResponse>> controllers) {
		this.controllers = controllers;
	}
	
	@Override
	protected void configure() {
		
		final TypeLiteral<Controller<DeleteAccountResponse>> deleteAccount = new TypeLiteral<Controller<DeleteAccountResponse>>() {};
		final Named deleteAccountName = Names.named(DeleteAccount.NAME);
		
		bind(deleteAccount).annotatedWith(deleteAccountName).to(DeleteAccount.class).in(Scopes.SINGLETON);
		
		this.controllers.addBinding(DeleteAccount.NAME).to(Key.get(deleteAccount, deleteAccountName)).in(Scopes.SINGLETON);
		
	}

}
