package com.bbva.practitioner.accounts.v1.rest.dto.response;

import javax.annotation.Nonnull;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public abstract class APIResponse {

	@Expose
	@Since(1.0)
	@SerializedName("success")
	@XStreamAlias("success")
	private final Boolean success;
	
	@Expose
	@Since(1.0)
	@SerializedName("message")
	@XStreamAlias("message")
	private final String message;
	
	protected APIResponse(final @Nonnull Boolean success, final @Nonnull String message) {
		this.message = Preconditions.checkNotNull(Strings.emptyToNull(message));
		this.success = success;
	}
	
}
