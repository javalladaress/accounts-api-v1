package com.bbva.practitioner.accounts.v1.rest.dto.response;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.google.common.base.MoreObjects;
import com.google.common.base.Supplier;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public abstract class AccountsAPIResponse extends APIResponse implements Supplier<Collection<Account>> {
	
	@Expose
	@Since(1.0)
	@SerializedName("account")
	@XStreamAlias("account")
	private Account account;
	
	@Expose
	@Since(1.0)
	@SerializedName("accounts")
	@XStreamAlias("accounts")
	private Collection<Account> accounts;

	private AccountsAPIResponse(final @Nonnull String message, final @Nonnull Boolean success) {
		super(success, message);
		this.account = null;
		this.accounts = null;
	}
	
	protected AccountsAPIResponse(final @Nonnull String message, 
			final @Nullable Account account, final @Nonnull Boolean success) {
		this(message, success);
		this.account = account;
	}
	
	protected AccountsAPIResponse(final @Nonnull String message, 
			final @Nullable Collection<Account> accounts, final @Nonnull Boolean success) {
		this(message, success);
		this.accounts = accounts;
	}
	
	@Override
	public Collection<Account> get() {
		return (this.account == null)? ((this.accounts == null)? Collections.emptyList() : this.accounts) : Collections.singleton(this.account);
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("accounts", get()).toString();
	}
	
}
