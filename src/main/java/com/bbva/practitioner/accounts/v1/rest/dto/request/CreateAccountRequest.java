package com.bbva.practitioner.accounts.v1.rest.dto.request;

import com.bbva.practitioner.accounts.v1.data.DaaSBodyProvider;
import com.bbva.practitioner.accounts.v1.data.dto.Account;
import com.google.common.base.Supplier;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Since;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CreateAccountRequest")
public class CreateAccountRequest implements Supplier<Account>, DaaSBodyProvider<Account> {

	@Expose
	@Since(1.0)
	@SerializedName("account")
	@XStreamAlias("account")
	private final Account account;
	
	private CreateAccountRequest(final Account account) {
		this.account = account;
	}
	
	public static CreateAccountRequest of(final Account account) {
		return new CreateAccountRequest(account);
	}
	
	@Override
	public Account getDaaSBody() {
		return get();
	}

	@Override
	public Account get() {
		return this.account;
	}

}
