package com.bbva.practitioner.accounts.v1.rest.get;

import com.bbva.practitioner.accounts.v1.rest.Controller;
import com.bbva.practitioner.accounts.v1.rest.dto.response.APIResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountActivityResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountTotalAmountResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetAccountsResponse;
import com.bbva.practitioner.accounts.v1.rest.dto.response.GetIndexResponse;
import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class GetRoutesModule extends AbstractModule {
	
	final MapBinder<String, Controller<? extends APIResponse>> controllers;
	
	public GetRoutesModule(final MapBinder<String, Controller<? extends APIResponse>> controllers) {
		this.controllers = controllers;
	}
	
	@Override
	protected void configure() {
		
		final TypeLiteral<Controller<GetIndexResponse>> getIndex = new TypeLiteral<Controller<GetIndexResponse>>() {};
		final TypeLiteral<Controller<GetAccountResponse>> getAccount = new TypeLiteral<Controller<GetAccountResponse>>() {};
		final TypeLiteral<Controller<GetAccountsResponse>> getAccounts = new TypeLiteral<Controller<GetAccountsResponse>>() {};
		
		final TypeLiteral<Controller<GetAccountActivityResponse>> getAccountActivity = new TypeLiteral<Controller<GetAccountActivityResponse>>() {};
		final TypeLiteral<Controller<GetAccountTotalAmountResponse>> getAccountTotalAmount = new TypeLiteral<Controller<GetAccountTotalAmountResponse>>() {};
		
		final Named getIndexName = Names.named(GetIndex.NAME);
		final Named getAccountName = Names.named(GetAccount.NAME);
		final Named getAccountsName = Names.named(GetAccounts.NAME);
		final Named getAccountActivityName = Names.named(GetAccountActivity.NAME);
		final Named getAccountTotalAmountName = Names.named(GetAccountTotalAmount.NAME);
		
		bind(getIndex).annotatedWith(getIndexName).to(GetIndex.class).in(Scopes.SINGLETON);
		bind(getAccount).annotatedWith(getAccountName).to(GetAccount.class).in(Scopes.SINGLETON);
		bind(getAccounts).annotatedWith(getAccountsName).to(GetAccounts.class).in(Scopes.SINGLETON);
		bind(getAccountActivity).annotatedWith(getAccountActivityName).to(GetAccountActivity.class).in(Scopes.SINGLETON);
		bind(getAccountTotalAmount).annotatedWith(getAccountTotalAmountName).to(GetAccountTotalAmount.class).in(Scopes.SINGLETON);
		
		this.controllers.addBinding(GetIndex.NAME).to(Key.get(getIndex, getIndexName)).in(Scopes.SINGLETON);
		this.controllers.addBinding(GetAccount.NAME).to(Key.get(getAccount, getAccountName)).in(Scopes.SINGLETON);
		this.controllers.addBinding(GetAccounts.NAME).to(Key.get(getAccounts, getAccountsName)).in(Scopes.SINGLETON);
		
		this.controllers.addBinding(GetAccountActivity.NAME).to(Key.get(getAccountActivity, getAccountActivityName)).in(Scopes.SINGLETON);
		this.controllers.addBinding(GetAccountTotalAmount.NAME).to(Key.get(getAccountTotalAmount, getAccountTotalAmountName)).in(Scopes.SINGLETON);
		
	}

}
